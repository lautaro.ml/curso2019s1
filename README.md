# Introduccción a los Sistemas Operativos

## 2019 Semestre 1

En este respositorio se irá publicando material de la materia (slides, código, tutoriales, etc).


## Listas

- tpi-est-so@listas.unq.edu.ar
        Dudas, consultas, avisos para comunicación con todos los estudiantes y docentes.

- tpi-doc-so@listas.unq.edu.ar
 Cualquier duda de la materia que no es para todos los compañeros, envíenla a esta lista



## Libros usados en la cursada

- Modern Operating Systems (Tanenaum)
- Operating System Concepts (Silberschatz)


## Slides de las Clases

- [0 - Curso](./slides/00_curso.pdf)
- [1 - Intro](./slides/01_intro.pdf)


## Trabajos Prácticos

Durante la materia iremos trabajando sobre un simulador básico de un sistema operativo que nos permita entender los conceptos que iremos desarrollando a lo largo de las clases. Como hemos mencionado, a esta altura carecemos de las herramientas para trabajar sobre un sistema operativo, aún de propósito didáctico como por ejemplo NachoOS. Por tal motivo elegimos trabajar con un simulador que nos permita enfocarnos en los conceptos estudiados.

- [Intro a Python](./python/python_intro.md)

Breve (muy breve) introducción a Python y al trabajo práctico.

- [Ejemplos](./python/examples)

Algunos ejemplos de Python utilizados en la Introducción.

## Prácticas
- [Práctica 1 - Un Simulador "Extremadamente" Simplificado.](./practicas/practica_1) 
<!---
- [Práctica 2 - Procesos - Clock - Interrupcion #KILL ](./practicas/practica_2) 
- [Práctica 3 - Multiprogramación ](./practicas/practica_3) 
- [Práctica 4 - Scheduler](./practicas/practica_4) 
- [Práctica 5 - Memoria: Paginación](./practicas/practica_5) 
- [Práctica 6 - Memoria: Paginación Bajo Demanda](./practicas/practica_6) 
-->


## Fechas de Entrega
<!--

| C1 | C2 | Practica | Path |
| ------ | ---   | ---------- | ---------  |
| 06/09 | 06/09 | Práctica 1 |  entregas/practica_1 |
| 13/09 | 17/09 | Práctica 2 |  entregas/practica_2 |
| 27/09 | 03/10 | Práctica 3 |  entregas/practica_3 |
| 18/10 | 18/10 | Práctica 4 |  entregas/practica_4 |
| 01/11 |       | Práctica 5 |  entregas/practica_5 |
| 15/11 | 15/11 | Parcial    |   |
| 01/12 |       | Práctica 6 |  entregas/practica_6 |
| 29/11 y 6/12 |       | Exposición  |  |
| 13/12 |       | Entrega Notas |  |
-->


<!--
## Fechas Presentación

| Dia | # Orden  | Grupo |
| ----  | --- | ---  |
| 29/11 | 1  | 1 |
| 29/11 | 2  | 5 |
| 29/11 | 3  | 10 |
| 29/11 | 4  | 9 |
| 29/11 | 5  | 11 |
| 29/11 | 6  | 7 |
| 29/11 | 7  | 6 |
| 29/11 | 8  | 3 |
|  6/12 | 1  | 4 |
|  6/12 | 2  | 15 |
|  6/12 | 3  | 8 |
|  6/12 | 4  | 14 |
|  6/12 | 5  | 12 |
|  6/12 | 6  | 13 |
|  6/12 | 7  | 16 |


-->
